// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

using System;
using System.IO;
using UnrealBuildTool;

public class OmniSDK : ModuleRules
{
	public OmniSDK(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
        bEnableUndefinedIdentifierWarnings = false;
        PrivateIncludePaths.AddRange(
			new string[] {
				"OmniSDK/Public"
				// ... add public include paths required here ...
			}
			);
				
		
		PrivateIncludePaths.AddRange(
			new string[] {
				"OmniSDK/Private",
				// ... add other private include paths required here ...
			}
			);
			
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
                "Core",
                "UMG",
                "HeadMountedDisplay",
                "InputCore",
                "InputDevice",
				// ... add other public dependencies that you statically link with here ...
			}
            );
			
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject",
				"Engine",
				"Slate",
				"SlateCore",
				// ... add private dependencies that you statically link with here ...	
			}
			);
		
		
		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
				// ... add any modules that your module loads dynamically here ...
			}
			);

        LoadYourThirdPartyLibraries(Target);
    }

    public bool LoadYourThirdPartyLibraries(ReadOnlyTargetRules Target)
    {
        string ModulePath = ModuleDirectory;
        string Plugin_dir = Path.Combine(ModuleDirectory, "../../");
        string MyLibraryPath = Path.Combine(ModulePath, "ThirdParty", "hidapi/windows/");
        PublicIncludePaths.Add(Path.Combine(MyLibraryPath));
        return true;
    }


    public string GetUProjectPath()
    {
        return Path.Combine(ModuleDirectory, "../../../../");
    }

}
