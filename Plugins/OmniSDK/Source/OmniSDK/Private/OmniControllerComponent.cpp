// Fill out your copyright notice in the Description page of Project Settings.

#include "OmniControllerComponent.h"
#include "OmniPrivatePCH.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Omni_WinReg.h"


// Sets default values for this component's properties
UOmniControllerComponent::UOmniControllerComponent()
	:CouplingPercentage(1)
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	bForceDeveloperMode = false;

	bAutoCorrectStartYaw = true;
	bStartYawCorrected = false;
	StartYawSet = false;
	StartYawDiff = 0;
	BackwardMovementMultiplier = 0.60f;
	StrafeSpeedMultiplier = 1.0f;
	AdjustedOmniYaw = 0.0f;

	CouplingPercentage = (FCString::Atof(*UOmniBPRegistryAccess::ReadRegistryValue("Software\\HeroVR\\SDK\\CouplingPercentage", "")));

	if (CouplingPercentage == 400)	//No value found
		CouplingPercentage = 1.0f;

	OmniYawOffset = FCString::Atof(*UOmniBPRegistryAccess::ReadRegistryValue("Software\\HeroVR\\SDK\\OmniYawOffset", ""));

	MovementDirection = FRotator(0.0f, 0.0f, 0.0f);
}


// Called every frame
void UOmniControllerComponent::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	APawn *pawn = Cast<APawn>(GetOwner());
	if (pawn)
	{
		AController *controller = pawn->GetController();
		if (controller && controller->IsLocalPlayerController())
		{
			// find Camera from Owner character;
			if (nullptr == Camera)
			{
				const TSet<UActorComponent*> &compos = pawn->GetComponents();
				for (UActorComponent *compo : compos)
				{
					if (compo 
						&& compo->IsA(UCameraComponent::StaticClass())
						&& (CameraTag == NAME_None || compo->ComponentHasTag(CameraTag)))
					{
						Camera = Cast<UCameraComponent>(compo);
						break;
					}
				}
			}

			if (!IsDeveloperMode())
			{
				// Raw yaw -> Game world yaw.
				UOmniControllerPluginFunctionLibrary::GetYaw(RawOmniYaw);

				if (RawOmniYaw > 180) {
					OmniYaw = RawOmniYaw - 360 - OmniYawOffset;
				}
				else {
					OmniYaw = RawOmniYaw - OmniYawOffset;
				}


				float cameraYaw = Camera ? Camera->GetComponentTransform().Rotator().Yaw : 0;

				//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

				//Calculating the Angle Between the Camera and the Omni

				APlayerController *pc = CastChecked<APlayerController>(controller);
				float CharacterRotation = pawn->GetActorTransform().Rotator().Yaw;

				AdjustedOmniYaw = (RawOmniYaw - OmniYawOffset + CharacterRotation);
				float AngleBetweenCameraAndOmni = (int)FMath::Abs(cameraYaw - AdjustedOmniYaw) % 360;

				AngleBetweenCameraAndOmni = AngleBetweenCameraAndOmni > 180 ? 360 - AngleBetweenCameraAndOmni : AngleBetweenCameraAndOmni;

				float AngleForSignCalc = (int)(cameraYaw - AdjustedOmniYaw) % 360;

				float sign = (AngleForSignCalc >= 0 && AngleForSignCalc <= 180) ||
					(AngleForSignCalc <= -180 && AngleForSignCalc >= -360) ? 1 : -1;

				AngleBetweenCameraAndOmni *= sign;

				//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

				CurrYaw = (RawOmniYaw - OmniYawOffset + CharacterRotation) + (AngleBetweenCameraAndOmni * CouplingPercentage);

				if (bAutoCorrectStartYaw && !StartYawSet)
				{
					// Only want to set this rotation once. Causes motion sickness if allowed to set on Tick.

					StartYawSet = true;
					float CameraForwardYaw = (RawOmniYaw - OmniYawOffset + CharacterRotation) + (AngleBetweenCameraAndOmni);
					float CameraOffsetFromCharacter = pawn->GetActorTransform().Rotator().Yaw - CameraForwardYaw;

					FRotator CameraOffsetRotator = FRotator(ForceInit);
					CameraOffsetRotator.Yaw = CameraOffsetFromCharacter;

					FRotator newRotation = FRotator(0.0f, 0.0f, 0.0f);
					newRotation = UKismetMathLibrary::ComposeRotators(CameraOffsetRotator, pc->GetControlRotation());

					pc->SetControlRotation(newRotation);
				}

				MovementDirection = FRotator(0.0f, CurrYaw, 0.0f);
			}				
		}
	}

}


bool UOmniControllerComponent::IsDeveloperMode() const
{
#if UE_BUILD_SHIPPING == true
	return false;
#endif
	return bForceDeveloperMode;
}

