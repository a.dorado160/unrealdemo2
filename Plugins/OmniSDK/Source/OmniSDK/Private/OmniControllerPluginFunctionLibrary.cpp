// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
//
#include "OmniControllerPluginFunctionLibrary.h"
#include "OmniPrivatePCH.h"
#include "OmniInputDevice.h"

UOmniControllerPluginFunctionLibrary::UOmniControllerPluginFunctionLibrary(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
}


void UOmniControllerPluginFunctionLibrary::ResetStepCount()
{
	if (FOmniSDKModule::Get().OmniInputDevice.IsValid())
	{
		FOmniSDKModule::Get().OmniInputDevice->bResetStepCount = true;
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Omni not initialized to Reset Setup Count. Try adding a delay before calling if calling at startup."));
	}
}


void UOmniControllerPluginFunctionLibrary::GetStepCount(int& Steps)
{
	if (!FOmniSDKModule::IsAvailable())
	{
		Steps = 0;
		return;
	}

	int currentSteps = FOmniSDKModule::Get().OmniInputDevice->iCurrentStepCount;

	if (currentSteps < 0) currentSteps = 0;
	Steps = currentSteps;
}


void UOmniControllerPluginFunctionLibrary::GetXAxis(float& XAxis)
{
	if (FOmniSDKModule::IsAvailable())
	{
		if (FOmniSDKModule::Get().OmniInputDevice.IsValid())
			XAxis = FOmniSDKModule::Get().OmniInputDevice->XAxis;
		else
			XAxis = 0.0f;			
	}
	else
		XAxis = 0.0f;
}


void UOmniControllerPluginFunctionLibrary::GetYAxis(float& YAxis)
{
	if (FOmniSDKModule::IsAvailable())
	{
		if (FOmniSDKModule::Get().OmniInputDevice.IsValid())
			YAxis = FOmniSDKModule::Get().OmniInputDevice->YAxis;
		else
			YAxis = 0.0f;
	}
	else
		YAxis = 0.0f;
}


void UOmniControllerPluginFunctionLibrary::GetYaw(float& Yaw)
{
	if (FOmniSDKModule::IsAvailable())
	{
		if (FOmniSDKModule::Get().OmniInputDevice.IsValid())
			Yaw = FOmniSDKModule::Get().OmniInputDevice->OmniYaw;
		else
			Yaw = 0.0f;
	}
	else
		Yaw = 0.0f;
}


void UOmniControllerPluginFunctionLibrary::IsOmniFound(bool& Found)
{
	if (FOmniSDKModule::IsAvailable())
	{
		if (FOmniSDKModule::Get().OmniInputDevice.IsValid())
			Found = FOmniSDKModule::Get().OmniInputDevice->bInitializationSucceeded;
		else
			Found = false;
	}
	else
		Found = false;
}
