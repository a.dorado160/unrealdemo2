// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "OmniSDK.h"
#include "Engine.h"
#include "IInputInterface.h"
#include "Runtime/InputDevice/Public/IInputDevice.h"
//#include "IInputDevice.h"

// You should place include statements to your module's private header files here.  You only need to
// add includes for headers that are used in most of your module's source files though.
#include "CoreUObject.h"
