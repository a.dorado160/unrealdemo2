// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#include "OmniSDK.h"
#include "OmniPrivatePCH.h"
#include "OmniInputDevice.h"
#include "SlateBasics.h"
#include "Runtime/Core/Public/Features/IModularFeatures.h"
#include "Runtime/InputDevice/Public/IInputDeviceModule.h"
#include "WIndows/WIndowsPlatformProcess.h"


#define LOCTEXT_NAMESPACE "FOmniSDKModule"
#define DeviceArrival 0x8000
#define DeviceChanged 0x0007



void FOmniSDKModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	IInputDeviceModule::StartupModule();
}


void FOmniSDKModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.

	FWindowsApplication* App = GetApplication();

	if (App)
		App->RemoveMessageHandler(Handler);
}


TSharedPtr<class IInputDevice> FOmniSDKModule::CreateInputDevice(const TSharedRef<FGenericApplicationMessageHandler>& InMessageHandler)
{
	UE_LOG(LogTemp, Warning, TEXT("Creating new Omni input Device"));

	FWindowsApplication* App = GetApplication();

	if (App)
		App->AddMessageHandler(Handler);

	OmniInputDevice = MakeShareable(new FOmniInputDevice(InMessageHandler));
	return TSharedPtr< class IInputDevice >(OmniInputDevice);

}


bool FWindowsHandler::ProcessMessage(HWND Hwnd, uint32 Message, WPARAM WParam, LPARAM LParam, int32& OutResult)
{
	if (Message == 0x0219)
	{
		switch ((int32)WParam)
		{
		case DeviceArrival: //0x8000
		case DeviceChanged: //0x0007
		{
			FOmniSDKModule ref = FOmniSDKModule::Get();
			if (ref.OmniInputDevice->tryingToReconnectOmni)
				return true;

			if (!ref.OmniInputDevice->OmniDisconnected)
				return true;

			if (ref.OmniInputDevice->tickerLoops >= 3)
				ref.OmniInputDevice->Init();

			return true;
		}
		default:
			return false;
		}
	}

	return false;
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FOmniSDKModule, OmniSDK)