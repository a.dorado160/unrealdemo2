#include "Omni_WinReg.h"
#include "OmniPrivatePCH.h"



FString UOmniBPRegistryAccess::ReadRegistryValue(const FString& KeyName, const FString& ValueName)
{
	HKEY hKey = nullptr;

	LONG Result = RegOpenKeyExW(HKEY_CURRENT_USER, *KeyName, 0, KEY_READ, (PHKEY)&hKey);

	if (Result != ERROR_SUCCESS)
	{
		UE_LOG(LogTemp, Warning, TEXT("There was an error opening the passed in Key."));
	
		return FString("400");
	}

	TCHAR Buffer[MAXIMUM_REPARSE_DATA_BUFFER_SIZE];
	DWORD BufferSize = sizeof(Buffer);
	HRESULT hResult = RegQueryValueEx(hKey, *ValueName, 0, nullptr, reinterpret_cast<LPBYTE>(Buffer), &BufferSize);

	if (hResult != ERROR_SUCCESS)
	{
		UE_LOG(LogTemp, Warning, TEXT("There was an error getting the value of the passed in Key."));
	
		return FString("400");
	}

	return FString(Buffer);

}
