#pragma once
#include "AllowWindowsPlatformTypes.h"
#include <Winreg.h>
#include "HideWindowsPlatformTypes.h"

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Omni_WinReg.generated.h"


UCLASS()
class UOmniBPRegistryAccess : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	static FString ReadRegistryValue(const FString& KeyName, const FString& ValueName);
};
